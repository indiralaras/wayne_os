%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='releases')

  %# --------------------------------------------------------------------------
  %# Releases
  %if not tpl_vars.get('data'):
    <h3><u>No test releases found.</u></h3>
  %else:
    <div id="divReleases" class="lefted">
      <table class="alternate_background">
        <tbody>
          <tr>
            <th class="headeritem lefted">Release</th>
          </tr>
          %for release in tpl_vars['data']:
            <tr>
              <td class="lefted">{{! release }}</td>
            </tr>
          %end
        </tbody>
      </table>
    </div>
  %end
%end

%rebase('master.tpl', title='releases', query_string=query_string, body_block=body_block)
