%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%import datetime

%from src import settings
%_root = settings.settings.relative_root

%# Method to create headers
%def toheader(header):
  %for h in header.split('_'):
    {{ h[0].upper() + h[1:] }}&nbsp;
  %end
%end


%# Test result link with tooltip
%def test_link(test_name):
  <a class="tooltip"
     href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{! query_string(add={'tests': test_name}) }}">
    {{ test_name }}
    <span style="width:300px">
      View test details and logs for {{ test_name }}.
    </span>
  </a>
%end


%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='summary')

  %# --------------------------------------------------------------------------
  %# Failures
  <div id="divFailures" align="center">
  %_td = tpl_vars['data']
  %if not _td.row_headers:
    <h3><u>No recent problems found.</u></h3>
  %else:
    <table style="width: 100%">
      <thead>
        <tr>
          <th class="headeritem centered" colspan={{ len(_td.col_headers) + 1 }}>
            <a href="{{ _root }}/summary/{{ tpl_vars['filter_tag'] }}{{! query_string() }}">
              Summary
            </a>
          </th>
        </tr>
        <tr>
          <th class="headeritem centered">
            %toheader(_td.row_label)
          </th>
          %for _field in _td.col_headers:
            %if _field == 'reason':
            <th class="headeritem lefted">
            %else:
            <th class="headeritem centered">
            %end
              %toheader(_field)
            </th>
          %end
        </tr>
      </thead>
      <tbody>
        %for _test_name in _td.row_headers:
          <tr>
            <td class="failure_table" onmouseover="fixTooltip(event, this)"
                                      onmouseout="clearTooltip(this)">
              %test_link(_test_name)
            </td>
            %for _field in _td.col_headers:
              %_cell = _td.get_cell(_test_name, _field)
              %if _cell.count == 0:
                &nbsp;
                %continue
              %end
              <td class="failure_table">
                %_data = _cell.get_sorted()
                %if isinstance(_data[0], datetime.datetime):
                  {{ _data[-1].strftime('%a %b %d, %H:%M') }}
                  %if len(_data) > 1:
                    ...
                  %end
                %elif len(_data) > 1:
                  <div class="lefted">
                    <ul>
                    %for r in _data:
                      <li>{{ r }}</li>
                    %end
                    </ul>
                  </div>
                %else:
                  %if _field == 'reason':
                    <div class="lefted">
                      {{ _data[-1] }}
                    </div>
                  %else:
                    {{ _data[-1] }}
                  %end
                %end
              </td>
            %end
          </tr>
        %end
      </tbody>
    </table>
  %end
  </div>
  <hr>
%end

%rebase('master.tpl', title='summary', query_string=query_string, body_block=body_block)
