# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""This is the main entry point for Whining Matrix dashboard on Google
App Engine.

Wmatrix uses bottle.py framework instaed of webapp2 for
request routing and template rendering.

Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
It enables simple webpage rendering from a single file (bottle.py) with no
other dependencies. See http://bottlepy.org/
"""

from src import settings
settings.settings = settings.Settings(settings.DEFAULT_CONFIG_INI)

import bottle
from src import urls


# In GAE absolute paths for bottle templates don't work.
bottle.TEMPLATE_PATH = ['./templates']

# DEBUG
bottle.debug(True)


# app is the WSGI aaplication object used by GAE. It is referenced as a URL
# handler in app.yaml as
#   script: run_appengine.app
app = bottle.Bottle()


# Map routes onto the app object.
urls.add_routes(app, './static')


# app.run() does not really run any server, just sets some params for GAE
# See http://bottlepy.org/docs/dev/deployment.html#google-appengine
app.run(server='gae')
