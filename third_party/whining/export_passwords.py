#!/usr/bin/python
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Script to export passwords from config files to the enviroment.

For use by wmatrix setup scripts.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from src import settings
from src.backend import update_lib


if __name__ == '__main__':
    options = update_lib.initialize_options()
    all_settings = settings.Settings(options.config)
    raw_settings = all_settings.get_db("raw")
    wmatrix_settings = all_settings.get_db("wmatrix")
    raw_password = raw_settings.pop("passwd")
    wmatrix_password = wmatrix_settings.pop("passwd")

    if raw_password != wmatrix_password:
        raise ValueError("Passwords must match.")
    print(raw_password)
