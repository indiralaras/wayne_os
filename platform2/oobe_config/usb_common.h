// Copyright 2018 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef OOBE_CONFIG_USB_COMMON_H_
#define OOBE_CONFIG_USB_COMMON_H_

namespace oobe_config {

extern const char kStatefulDir[];
extern const char kUnencryptedOobeConfigDir[];
extern const char kConfigFile[];
extern const char kDomainFile[];
extern const char kKeyFile[];
extern const char kDevDiskById[];
extern const char kUsbDevicePathSigFile[];

}  // namespace oobe_config

#endif  // OOBE_CONFIG_USB_COMMON_H_
