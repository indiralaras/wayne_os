/*
 * Copyright 2018 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef CAMERA_HAL_ADAPTER_VENDOR_TAGS_H_
#define CAMERA_HAL_ADAPTER_VENDOR_TAGS_H_

namespace cros {

// Hierarchy positions in enum space
typedef enum vendor_section_start {
  VENDOR_GOOGLE_START = VENDOR_SECTION_START
} vendor_section_start_t;

}  // namespace cros

#endif  // CAMERA_HAL_ADAPTER_VENDOR_TAGS_H_
