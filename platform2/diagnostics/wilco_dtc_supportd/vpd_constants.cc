// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "diagnostics/wilco_dtc_supportd/vpd_constants.h"

namespace diagnostics {

// VPD field serial number file path.
const char kVpdFieldSerialNumberFilePath[] =
    "run/wilco_dtc/vpd_fields/serial_number";

}  // namespace diagnostics
