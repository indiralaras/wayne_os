// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// AUTOGENERATED FILE
//
// This file is autogenerated! If you need to modify it, be sure to
// modify the script that exports Google voice data for use in Chrome.

// Initialize the voice array if it doesn't exist so that voice data files
// can be loaded in any order.

if (!window.voices) {
  window.voices = [];
}

// Add this voice to the global voice array.
window.voices.push({
  'pipelineFile': '/remote_voice_lstm_fi-FI/afi/pipeline',
  'prefix': '/remote_voice_lstm_fi-FI/afi/',
  'voiceType': 'lstm',
  'cacheToDisk': true,
  'lang': 'fi-FI',
  'displayName': 'Finnish',
  'voiceName': 'Chrome OS Suomi',
  'removePaths': [],
  'files': [
    {
      'path': '/remote_voice_lstm_fi-FI.zvoice',
      'url': 'https://redirector.gvt1.com/edgedl/android/tts/v16/fi-fi-r5.zvoice',
      'md5sum': '95eb00db19595c24e95a8b03e0a94c1e',
      'size': 6872420,
    },
  ],
});
