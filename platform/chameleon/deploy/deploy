#!/bin/sh
# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

SCRIPT="$(readlink -f $0)"
SCRIPT_DIR="$(dirname ${SCRIPT})/init.d"

INITD_DIR='/etc/init.d'
CHAMELEOND_DIR='/usr/bin'
CHAMELEOND_NAME='chameleond'
DISPLAYD_NAME='displayd'
STREAM_SERVER_NAME='stream_server'
SCHEDULER_NAME='scheduler'
UPDATER_NAME='chameleon-updater'
LIGHTTPD_NAME='lighttpd'

# We do not want lighttpd and chameleon-updater to run on startup.
update-rc.d -f ${LIGHTTPD_NAME} remove
update-rc.d -f ${UPDATER_NAME} remove

install_daemon_and_config () {
    DAEMON_NAME=$1
    CONFIG_FILE="/etc/default/${DAEMON_NAME}"
    DAEMON="${CHAMELEOND_DIR}/run_${DAEMON_NAME}"

    if [[ ! -x ${DAEMON} ]]; then
        echo "Executable ${DAEMON} not existed. Install it first." 1>&2
        exit 1
    fi

    update-rc.d -f ${DAEMON_NAME} remove

    CONFIG_TMP="$(mktemp /tmp/${DAEMON_NAME}.XXXXXX)"
    cat <<END >${CONFIG_TMP}
CHAMELEOND_DIR="${CHAMELEOND_DIR}"
BUNDLE_VERSION="${BUNDLE_VERSION}"
CHAMELEON_BOARD="${CHAMELEON_BOARD}"
END
    mv -f "${CONFIG_TMP}" "${CONFIG_FILE}"
}

mkdir -p ${INITD_DIR}
install_daemon_and_config "${CHAMELEOND_NAME}"
install_daemon_and_config "${DISPLAYD_NAME}"
install_daemon_and_config "${STREAM_SERVER_NAME}"
install_daemon_and_config "${SCHEDULER_NAME}"

cp -f "${SCRIPT_DIR}/${CHAMELEOND_NAME}" "${INITD_DIR}"
update-rc.d "${CHAMELEOND_NAME}" defaults 92 8

cp -f "${SCRIPT_DIR}/${DISPLAYD_NAME}" "${INITD_DIR}"
update-rc.d "${DISPLAYD_NAME}" defaults 94 6

cp -f "${SCRIPT_DIR}/${STREAM_SERVER_NAME}" "${INITD_DIR}"
update-rc.d "${STREAM_SERVER_NAME}" defaults 96 4

cp -f "${SCRIPT_DIR}/${SCHEDULER_NAME}" "${INITD_DIR}"
update-rc.d "${SCHEDULER_NAME}" defaults 98 4

cp -f "${SCRIPT_DIR}/${UPDATER_NAME}" "${INITD_DIR}"
update-rc.d "${UPDATER_NAME}" defaults 90 10

FPGA_DIR="$(dirname ${SCRIPT})/../updatable/${CHAMELEON_BOARD}"
FPGA_BOOT_PART=$(fdisk -l /dev/mmcblk0 | grep 'Unknown' | cut -f1 -d' ')
FPGA_FAT_PART=$(fdisk -l /dev/mmcblk0 | grep 'FAT32' | cut -f1 -d' ')
FPGA_LINUX_PART=$(fdisk -l /dev/mmcblk0 | grep 'Linux' | cut -f1 -d' ')
MOUNT_DIR='/media/mmc1'

mkdir -p ${MOUNT_DIR}
mount "${FPGA_FAT_PART}" "${MOUNT_DIR}"
for file in 'fpga.rbf' 'socfpga.dtb' 'zImage'; do
    if ! diff -q "${FPGA_DIR}/${file}" "${MOUNT_DIR}/${file}"; then
        echo "Updating ${file}"
        cp -f "${FPGA_DIR}/${file}" "${MOUNT_DIR}"
        need_reboot=1
    fi
done
umount "${MOUNT_DIR}"

mount "${FPGA_LINUX_PART}" "${MOUNT_DIR}"
MODULE_DIR="${MOUNT_DIR}/lib/modules"
file="${FPGA_DIR}/modules.tgz"
if [[ -e "${file}" ]]; then
    new_modules_version="$(tar tf "${file}" |head -n 1)"
    new_modules_version="${new_modules_version%?}"
    echo "new_modules_version ${new_modules_version}"
    current_modules_version="$(ls -1 "${MODULE_DIR}" |head -n 1)"
    echo "current_modules_version ${current_modules_version}"
    if [ "${new_modules_version}" != "${current_modules_version}" ]; then
        echo "Updating ${file}"
        rm -rf "${MODULE_DIR}"/*
        tar zxvf "${file}" -C "${MODULE_DIR}"
        need_reboot=1
    fi
fi
umount "${MOUNT_DIR}"

file='boot-partition.img'
if ! diff -q "${FPGA_DIR}/${file}" "${FPGA_BOOT_PART}"; then
    echo "Updating ${file}"
    dd if="${FPGA_DIR}/${file}" of="${FPGA_BOOT_PART}" bs=512
    sync
    need_reboot=1
fi

if [[ -n "${need_reboot}" ]]; then
    echo 'Reboot the board to validate the FPGA configuration...'
    reboot
else
    "${INITD_DIR}/${CHAMELEOND_NAME}" restart
    "${INITD_DIR}/${DISPLAYD_NAME}" restart
    "${INITD_DIR}/${STREAM_SERVER_NAME}" restart
    "${INITD_DIR}/${SCHEDULER_NAME}" restart
    if [[ -e "${INITD_DIR}/${LIGHTTPD_NAME}" ]]; then
        "${INITD_DIR}/${LIGHTTPD_NAME}" stop
    fi
fi
