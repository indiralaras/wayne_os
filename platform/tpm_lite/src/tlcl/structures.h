/* This file is automatically generated */

struct {
  uint8_t buffer[34];
  uint8_t* pcrNum;
  uint8_t* inDigest;
} tpm_extend_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x22, 0x0, 0x0, 0x0, 0x14, },
tpm_extend_cmd.buffer + 10, tpm_extend_cmd.buffer + 14, };

struct {
  uint8_t buffer[22];
  uint8_t* index;
} tpm_getpermissions_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x16, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0x11, 0x0, 0x0, 0x0, 0x4, },
tpm_getpermissions_cmd.buffer + 18, };

struct {
  uint8_t buffer[22];
} tpm_getflags_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x16, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x1, 0x8, },
};

struct {
  uint8_t buffer[11];
  uint8_t* deactivated;
} tpm_physicalsetdeactivated_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xb, 0x0, 0x0, 0x0, 0x72, },
tpm_physicalsetdeactivated_cmd.buffer + 10, };

struct {
  uint8_t buffer[10];
} tpm_physicalenable_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x6f, },
};

struct {
  uint8_t buffer[10];
} tpm_physicaldisable_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x70, },
};

struct {
  uint8_t buffer[10];
} tpm_forceclear_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x5d, },
};

struct {
  uint8_t buffer[30];
} tpm_readpubek_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x1e, 0x0, 0x0, 0x0, 0x7c, },
};

struct {
  uint8_t buffer[10];
} tpm_continueselftest_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x53, },
};

struct {
  uint8_t buffer[10];
} tpm_selftestfull_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xa, 0x0, 0x0, 0x0, 0x50, },
};

struct {
  uint8_t buffer[12];
} tpm_startup_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xc, 0x0, 0x0, 0x0, 0x99, 0x0, 0x1, },
};

struct {
  uint8_t buffer[12];
} tpm_pplock_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xc, 0x40, 0x0, 0x0, 0xa, 0x0, 0x4, },
};

struct {
  uint8_t buffer[12];
} tpm_ppassert_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0xc, 0x40, 0x0, 0x0, 0xa, 0x0, 0x8, },
};

struct {
  uint8_t buffer[22];
  uint8_t* index;
  uint8_t* length;
} tpm_nv_read_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x16, 0x0, 0x0, 0x0, 0xcf, },
tpm_nv_read_cmd.buffer + 10, tpm_nv_read_cmd.buffer + 18, };

struct {
  uint8_t buffer[256];
  uint8_t* index;
  uint8_t* length;
  uint8_t* data;
} tpm_nv_write_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xcd, },
tpm_nv_write_cmd.buffer + 10, tpm_nv_write_cmd.buffer + 18, tpm_nv_write_cmd.buffer + 22, };

struct {
  uint8_t buffer[101];
  uint8_t* index;
  uint8_t* perm;
  uint8_t* size;
} tpm_nv_definespace_cmd = {{0x0, 0xc1, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0xcc, 0x0, 0x18, 0, 0, 0, 0, 0x0, 0x3, 0, 0, 0, 0x1f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0, 0x3, 0, 0, 0, 0x1f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0, 0x17, },
tpm_nv_definespace_cmd.buffer + 12, tpm_nv_definespace_cmd.buffer + 70, tpm_nv_definespace_cmd.buffer + 77, };

const int kWriteInfoLength = 12;
const int kNvDataPublicPermissionsOffset = 60;
