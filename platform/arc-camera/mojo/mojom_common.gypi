{
  'variables': {
    'mojom_bindings_generator': '<(sysroot)/usr/src/libmojo-<(libbase_ver)/mojo/mojom_bindings_generator.py',
    'mojom_templates_gen_dir': '<(SHARED_INTERMEDIATE_DIR)/mojom_templates',
  },
}
