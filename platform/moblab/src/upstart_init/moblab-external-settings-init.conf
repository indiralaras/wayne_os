# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Mount and setup moblab settings external settings sd card."
author        "chromium-os-dev@chromium.org"

start on started cros-disks

normal exit 0

env MOUNT_DIR=/mnt/moblab-settings

script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  logger -t "${UPSTART_JOB}" "Starting"
  # REQUIRES A USB DRIVE WITH LABEL "MOBLAB-SETTINGS"
  sleep 1
  # It has been observed readlink fails occasionally
  # If readlink fails set usb_drive to an error message so the script will
  # continue assuming no external storage.
  usb_drive=$(readlink -f /dev/disk/by-label/MOBLAB-SETTINGS) ||
    echo "Readlink failed code $?"
  if [ -e "${usb_drive}" ]; then
    logger -t "${UPSTART_JOB}" "Mounting external settings ${usb_drive}."
    umount -A "${usb_drive}" || :
    /sbin/e2fsck -p "${usb_drive}" || :
    if ! mount "${usb_drive}" "${MOUNT_DIR}" -o exec,dev,nosuid; then
      logger -t "${UPSTART_JOB}" "Mounting of ${usb_drive} onto ${MOUNT_DIR}"
      logger -t "${UPSTART_JOB}" "failed. Please reconnect drive and reboot."
      exit 1
    fi
    chown moblab:moblab "${MOUNT_DIR}"
  fi
end script
