# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Clean board packages after libc++ migration.
for board_root in /build/* ; do
  board=${board_root##*/}
  [[ "${board}" == "bin" ]] && continue
  # Clean local portage caches. This is necessary if
  # there are cros_workon packages.
  sudo rm -rf ${board_root}/var/cache/portage/*

  # Find packages using libstdc++.
  PKGS=( $(qdepends-${board} -qCN -k NEEDED -Q 'libstdc\+\+\.so\.6') )
  if [[ ${#PKGS[@]} -eq 0 ]]; then
    continue
  fi
  echo "Cleaning packages using libstdc++ from ${board}"
  # Collect list of packages to remove excluding gcc-libs and binutils.
  PKG_TO_REMOVE=()
  for pkg in "${PKGS[@]}"; do
    if [[ "${pkg}" != "sys-libs/gcc-libs" && "${pkg}" != "sys-devel/binutils" ]]; then
      PKG_TO_REMOVE+=( "${pkg}" )
    fi
  done

  if [[ ${#PKG_TO_REMOVE[@]} -eq 0 ]]; then
    continue
  fi

  PKGDIR=$(portageq-${board} envvar PKGDIR)
  cd "${PKGDIR}"
  # Remove existing binary packages.
  for pkg in "${PKG_TO_REMOVE[@]}"; do
    sudo rm -f "${pkg}"-[0-9]*.tbz2
  done
  # Unmerge all packages using libstdc++.
  qmerge-${board} -Uqy "${PKG_TO_REMOVE[@]}"
done
